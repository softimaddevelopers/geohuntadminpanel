import { TestBed } from '@angular/core/testing';

import { AventureService } from './aventure.service';

describe('AventureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AventureService = TestBed.get(AventureService);
    expect(service).toBeTruthy();
  });
});
