import { Injectable } from '@angular/core';
import { Joueur } from "./../models/joueur.model";
import { AngularFireDatabase } from "angularfire2/database";

@Injectable({
  providedIn: 'root'
})
export class JoueurService {

  constructor(
  	private db:AngularFireDatabase
  	) { }

  getPaiements(idAventure){
  	return this.db.list("paiement/", function(ref) {
  		return ref.orderByChild("aventure_id").equalTo(idAventure);
  	}
  	).snapshotChanges();
  }

}
