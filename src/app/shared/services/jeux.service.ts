import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Jeux } from "./../models/jeux.model";
import { AngularFireDatabase } from "angularfire2/database";


@Injectable({
  providedIn: 'root'
})
export class JeuxService {

  constructor(
  	public db: AngularFireDatabase
  	) { }

  getJeux(idAventure){
  	return this.db.list("/jeux", function(ref) {
  		return ref.orderByChild("aventures_id").equalTo(idAventure);
  	}
  	).snapshotChanges();
  }

  set(path:string, data:Object){
    return this.db.database.ref(path).set(data);
  }

}
