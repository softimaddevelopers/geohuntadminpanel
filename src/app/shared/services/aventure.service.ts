import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Aventure } from "./../models/aventure.model";
import { AngularFireDatabase } from "angularfire2/database";
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AventureService {
  task: firebase.storage.UploadTask;
  constructor(
  	public db: AngularFireDatabase
  	) { }

  getAventures(){
  	return this.db.list("/aventures").snapshotChanges();
  }

  getData(path:string){
  	return this.db.database.ref(path)
  }
  create(path:string,data:Object=null){
  	 return this.db.list(path).push(data)
  }
  update(path:string,id:string,data:Object){
  	return this.db.list(path).update(id, data);
  }
  set(path:string,id:string,data:Object){
    return this.db.list(path).set(id, data);
  }

  delete(key){
    let self = this;
  	return this.db.database.ref("/jeux").child('aventures_id').equalTo(key).once('value', function(snapshot){
      snapshot.forEach(function(child){
        self.db.database.ref("jeux/"+child.key).remove();
      })

      self.db.database.ref('aventures/'+key).remove();

    });
  }
  pushFile(path :string ='/jeux',file:any){
    let sotrageRef = firebase.storage().ref();
    this.task = sotrageRef.child(path+'/image').put(file);
    return this.task;
  }
}
