import { Deserializable } from "./deserializable.model";
import { Lang, LangError } from "./lang.model";

export class Aventure implements Deserializable{
	key:string;
	titre:Lang = new Lang();
	description:Lang = new Lang();
	avatar_url:string="https://firebasestorage.googleapis.com/v0/b/geohunt-afa96.appspot.com/o/shiva.jpg?alt=media&token=7edd5c39-3478-4d59-9c39-20645aee97c5";
	temps_min:Number;
	temps_max:Number;
	distance:Number;
	prix:Number;
	niveau:string;
	lieu:string;
	status:string = "brouillon";
	transports:any= [];
	language:any={};

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

export class AventureError implements Deserializable{

	titre:LangError = new LangError();
	description:LangError = new LangError();
	temps_min:boolean;
	temps_max:boolean;
	distance:boolean;
	prix:boolean;
	niveau:boolean;
	lieu:boolean;

	transports:boolean;
	language:boolean;

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}

