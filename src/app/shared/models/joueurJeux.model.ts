export interface idJoueur{
	date_deb_aventure:string;
	date_fin_aventure:string;

}
export class JoueurJeux implements idJoueur{
	date_deb_aventure:string;
	date_fin_aventure:string;
	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}
