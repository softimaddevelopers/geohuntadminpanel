import { Deserializable } from "./deserializable.model";
import { Lang } from "./lang.model";

export class Joueur implements Deserializable{

	firstname:string;
	lastname:string;
	email:string;
	key:string;
	

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}



