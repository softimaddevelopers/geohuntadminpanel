import { Deserializable } from "./deserializable.model";
import { Lang } from "./lang.model";
import { Aventure } from "./aventure.model";

export class Jeux implements Deserializable{
	key:string;
	titre:Lang = new Lang();
	description:Lang = new Lang();
	image_url:string;
	type:string;
	longitude:Number;
	latitude:Number;
	question:Lang = new Lang();
	reponse:Lang = new Lang();
	texte_qrcode:string;
	requis:Boolean=false;
	indices:Array<Indices>=[];
	penalisation_passer:Number;
	sequence:Number;
	aventures_id:String;
	actif:boolean=false;
	errors:string[];
	aventure:Aventure;

	constructor(aventure:Aventure){
		this.aventure = aventure;
	}


	setAventure(aventure:Aventure){
		this.aventure = aventure;
	}

	checkQuizz(){
		this.errors = [];
		let res = true;
		for(let i in this.aventure.language){
			if(this.aventure.language[i]){
				//check qest 
					if(!this.question[i]){
						this.errors.push("Question "+i+" requise");
						res = false;
					}
				//check rep
					if(!this.reponse[i]){
						this.errors.push("Réponse "+i+" requise");
						res = false;
					}

			}
		}

		if(!this.checkTempsRequis()){
			res = false;
		}

		if(!this.checkIndices()){
			res = false;
		}

		return res;
	}

	checkPuzzle(){
		this.errors = [];
		let res = true;
		for(let i in this.aventure.language){
			if(this.aventure.language[i]){
				//check qest 
					if(!this.titre[i]){
						this.errors.push("Titre "+i+" requise");
						res = false;
					}
				//check rep
					if(!this.description[i]){
						this.errors.push("Description "+i+" requise");
						res = false;
					}

			}
		}
		if(!this.image_url){
			this.errors.push("Image puzzle requise");
			res = false;
		}
		if(!this.checkTempsRequis()){
			res = false;
		}
		return res;

	}

	checkQrcode(){
		this.errors = [];
		let res = true;
		if(!this.texte_qrcode){
			this.errors.push("Texte QRCODE obligatoire");
			res = false;
		}
		if(!this.checkTempsRequis()){
			res = false;
		}
		if(!this.checkIndices()){
			res = false;
		}
		return res;
	}

	checkPhoto(){
		this.errors = [];
		let res = true;
		for(let i in this.aventure.language){
			if(this.aventure.language[i]){
				//check qest 
					if(!this.titre[i]){
						this.errors.push("Titre "+i+" requise");
						res = false;
					}
				//check rep
					if(!this.description[i]){
						this.errors.push("Description "+i+" requise");
						res = false;
					}

			}
		}

		if(!this.checkTempsRequis()){
			res = false;
		}
		if(!this.checkIndices()){
			res = false;
		}
		return res;
	}

	checkGeoloc(){
		this.errors = [];
		let res = true;
		if(!this.latitude){
			this.errors.push("Latitude gps obligatoire");
			res = false;
		}
		if(!this.longitude){
			this.errors.push("Longitude gps obligatoire");
			res = false;
		}

		for(let i in this.aventure.language){
			if(this.aventure.language[i]){
				//check qest 
					if(!this.titre[i]){
						this.errors.push("Titre "+i+" requise");
						res = false;
					}
				//check rep
					if(!this.description[i]){
						this.errors.push("Description "+i+" requise");
						res = false;
					}

			}
		}
		if(!this.checkTempsRequis()){
			res = false;
		}
		if(!this.checkIndices()){
			res = false;
		}
		return res;


	}
	checkTempsRequis(){
		let res = true;
		if(!this.requis && !this.penalisation_passer){
			this.errors.push("Configuration : Temps de pénalisation obligatoire");
			res = false;
		}
		return res;
	}

	checkIndices(){
		let res = true;
		for(let i in this.aventure.language){
			if(this.aventure.language[i]){
				for(let j in this.indices){
					if(!this.indices[j].indice[i]){
						res = false;
						this.errors.push("Indice "+j+": Champ indice obligatoire");
					}
				}
			}
		}
				
		return res;
	}

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}


	// checkTexte_qrcode(){
	// 	if (!this.texte_qrcode && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkTitre(){
	// 	if (!this.titre && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }

	// checkDescription(){
	// 	if (!this.description && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }

	// checkImage_url(){
	// 	if (!this.image_url && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkPenalisation_passer(){
	// 	if (!this.penalisation_passer && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkRequis(){
	// 	if (!this.requis && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkQuestion_fr(){
	// 	if (!this.question_fr && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkQuestion_en(){
	// 	if (!this.question_en && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkReponse_en(){
	// 	if (!this.reponse_en && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkReponse_fr(){
	// 	if (!this.reponse_fr && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkIndicesParIndices(i:number){
	// 	if (!this.indices[i].indice && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkIndicesParPenalisation(i:number){
	// 	if (!this.indices[i].temps_penalisation && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkLongitude(){
	// 	if (!this.longitude && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// checkLatitude(){
	// 	if (!this.latitude && this.actif) {
	// 		return false;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }
	// testIndicesParIndice(){
	// 	let compteurIndicesFalse:number=0;
	// 	for (var i = 0; i < this.indices.length; i++) {
	// 		if(!this.indices[i].indice) compteurIndicesFalse++
	// 	}
	// 	if (compteurIndicesFalse >0) {
	// 		// code...
	// 		return false;
	// 	}
	// 	else{
	// 		return true;
	// 	}
	// }
	// testIndicesParPenalisation(){
	// 	let compteurIndicesFalse:number=0;
	// 	for (var i = 0; i < this.indices.length; i++) {
	// 		if(!this.indices[i].temps_penalisation) compteurIndicesFalse++
	// 	}
	// 	if (compteurIndicesFalse >0) {
	// 		// code...
	// 		return false;
	// 	}
	// 	else{
	// 		return true;
	// 	}
	// }
}

export class Indices{
	indice:Lang = new Lang();
	temps_penalisation:number = 360;
}