import { Deserializable } from "./deserializable.model";
import { Lang } from "./lang.model";

export class Home implements Deserializable{

	titre:Lang = new Lang();
	description:Lang = new Lang();
	

	deserialize(input: any) {
	    Object.assign(this, input);
	    return this;
	}
}



