import { CommonModule } from '@angular/common';
import { environment } from '../environments/environment';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { ImageCropperModule } from 'ngx-image-cropper';
import {AngularFirestoreModule} from 'angularfire2/firestore'
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule} from '@angular/forms';
import {NgDragDropModule} from 'ng-drag-drop';
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        NgxLoadingModule.forRoot({}),
        BrowserModule,
        FormsModule,
        AngularFireDatabaseModule,
        AngularFireStorageModule,
        AngularFireAuthModule,
        ImageCropperModule,
        NgDragDropModule.forRoot(),
        AngularFirestoreModule,
        AngularFireModule.initializeApp(environment.firebase),
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, AngularFireStorageModule, AngularFireDatabaseModule, AngularFireAuthModule],
    bootstrap: [AppComponent]
})
export class AppModule {}
