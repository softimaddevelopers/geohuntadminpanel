import { TestBed } from '@angular/core/testing';

import { AventureService } from './puzzle-service.service';

describe('PuzzleServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AventureService = TestBed.get(AventureService);
    expect(service).toBeTruthy();
  });
});
