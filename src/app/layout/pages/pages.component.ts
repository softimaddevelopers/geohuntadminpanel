import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { Lang } from "../../shared/models/lang.model";
import { Home } from "../../shared/models/home.model";

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})



export class PagesComponent implements OnInit {


	contact:Lang = new Lang();
	termes:Lang = new Lang();
	home:Home = new Home();
	faq:Lang = new Lang();;

	message:string=""
  constructor(
  	private db: AngularFireDatabase
  	) { 
 
  }

  ngOnInit() {
  	let self = this;
  	self.message = "";
  	this.db.object("termes").snapshotChanges().subscribe(function(snapshot){
  		if(snapshot.payload.val()){
  			self.termes = new Lang().deserialize(snapshot.payload.val());
  		}
  		
  		
  	})



  	this.db.object("home").snapshotChanges().subscribe(function(snapshot){
  		if(snapshot.payload.val()){
  			self.home = new Home().deserialize(snapshot.payload.val());
  		}
  		
  		
  	})

  	this.db.object("contact").snapshotChanges().subscribe(function(snapshot){
  		if(snapshot.payload.val()){
  			self.contact =new Lang().deserialize(snapshot.payload.val());
  		}
  		
  		
  	})

  	this.db.object("faq").snapshotChanges().subscribe(function(snapshot){
  		if(snapshot.payload.val()){
  			self.faq = new Lang().deserialize(snapshot.payload.val());
  		}
  		
  		
  	})
  }

  saveTermes(){
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  	let self = this;
  	this.db.database.ref("termes").set(this.termes).then(function(){
  		self.message = "Les termes et conditions ont été mis à jour";
  	});

  }

  saveHome(){
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  	let self = this;
  	this.db.database.ref("home").set(this.home).then(function(){
  		self.message = "La page d'accueil a été mise à jour";
  	});

  }

  saveContact(){
  	let self = this;
  	this.db.database.ref("contact").set(this.contact).then(function(){
  		self.message = "La page contact a été mise à jour";
  	});

  }

  saveFaq(){
  	let self = this;
  	this.db.database.ref("faq").set(this.faq).then(function(){
  		self.message = "La page FAQ a été mise à jour";
  	});

  }


}
