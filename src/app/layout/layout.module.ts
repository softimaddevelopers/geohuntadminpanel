import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { AventuresComponent } from './aventures/aventures.component';
import { CreeAventureComponent } from './cree-aventure/cree-aventure.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';
import { ConfigurationComponent } from './configuration/configuration.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PagesComponent } from './pages/pages.component' ;
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { NgxEditorModule } from 'ngx-editor';
import { PaiementComponent } from './paiement/paiement.component';
import { NgDragDropModule } from 'ng-drag-drop';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        DragDropModule,
        NgDragDropModule.forRoot(),
        NgxLoadingModule.forRoot({
        	fullScreenBackdrop:false
        }),
        NgbDropdownModule,
        NgbModule,
        FormsModule,
        ImageCropperModule,
        // FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        NgxEditorModule,
        LeafletModule.forRoot()

    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, AventuresComponent, CreeAventureComponent, ConfigurationComponent, LeaderboardComponent, PagesComponent, PaiementComponent]
})
export class LayoutModule {}
