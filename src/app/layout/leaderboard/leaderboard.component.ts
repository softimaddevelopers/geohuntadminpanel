import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AngularFireDatabase } from "angularfire2/database";
import { Jeux } from './../../shared/models/jeux.model';
import { Aventure } from './../../shared/models/aventure.model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

	idAventure:String;
	loading:boolean=false;// afficher loader
	aventure:Aventure = new Aventure();
  joueurs:any=[];
  joueurJeux:any=[];
  joueurJeuxClone:any=[];

  photos:any=[];
  closeResult: string;

  photo:string;
  showButtonRang:boolean=false;

  	constructor(
  		public route: ActivatedRoute,
  		public router: Router,
  		public db:AngularFireDatabase,
      private modalService: NgbModal,
  		) {
  		this.idAventure = this.route.snapshot.paramMap.get('idAventures');  
      
  	}


  	ngOnInit() {
  		let self = this;
        
        /*this.db.list("joueurJeux/"+this.idAventure).query.orderByChild('temps_total').on('value',(snap)=>{
          console.log('list orderby', snap.val());
        })*/
        /*this.db.database.ref("joueurJeux/"+this.idAventure)
            .on('value',(snapshot)=>{
                console.log('this is snapshot order by:',snapshot.val());
            })*/

	     this.db.object("aventures/"+this.idAventure).snapshotChanges().subscribe((data) =>{
	
	       self.aventure = new Aventure().deserialize({ key: data.key, ...data.payload.val() });
         this.loading = false;
         this.loadJoueurJeux('rang');

	       
	     });
       this.db.object("joueurJeuxPhoto/"+this.idAventure).snapshotChanges().subscribe((data) =>{
  
         self.photos = data.payload.val();
         
       })



  	}
    loadJoueurJeux(byWhat='temps_total'){
      let self = this;

      this.loading = true;

      if(byWhat =='temps_total'){ 
        this.showButtonRang = true;
      }
      else{
        this.showButtonRang = false;
      }

      this.db.object('joueur').snapshotChanges().subscribe((data)=>{
         self.joueurs = data.payload.val();
         //console.log('joueurs',self.joueurs);
       });
      this.db.object("joueurJeux/"+this.idAventure).snapshotChanges().subscribe((data)=>{
          let labels=[];
          self.loading = false;
          for(var key in data.payload.val()){
            ///alert('a');
            let clone:any= Object.assign({},data.payload.val()[key])
            clone.key = key;
            labels.push(key);
            self.joueurJeuxClone.push(clone);
          }
          console.log('hanao trie');
          self.tri(self.joueurJeuxClone,byWhat);
          self.joueurJeuxClone = self.joueurJeuxClone.slice().reverse();
          self.joueurJeux = self.joueurJeuxClone;
          self.joueurJeuxClone=[];
          /*console.log('avy ntriena reverse');
          console.log(self.joueurJeuxClone.slice().reverse());*/
          
        })
    }
  	contenu(){
	    this.router.navigate(['creerAventures/'+this.idAventure]);
	}

	configuration(){
	    this.router.navigate(['configurationAventure/'+this.idAventure]);
	}
  paiement(){
      this.router.navigate(['paiement/'+this.idAventure]);
  }
  tri(dataArray:any=[],byTemps_total='temps_total',n:number=null){
    if(n==null) n = dataArray.length;
    if(n==0){
      return dataArray;
    }
    else{
      let temp_max=0;
      let toShiftData=null;
      for(var i=0; i<n;i++){
        if(dataArray[i][byTemps_total] > temp_max){
          temp_max = dataArray[i][byTemps_total]
          toShiftData= dataArray[i];
        }
      }
      toShiftData = dataArray.splice(dataArray.indexOf(toShiftData),1);
      dataArray.push(toShiftData[0]);
      return this.tri(dataArray,byTemps_total,n-1);
    }
    //console.log(dataArray);
  }
  onItemDrop(e){
    //console.log('mdroppe isika izao');
    let data= e.dragData;
    
    //console.log(e.previousIndex+''+e.currentIndex);
  }
  onItemDrag(e){
    //console.log('mdragg isika izao')
    //console.log(e);
  }
  drop(event: CdkDragDrop<Array<any>>) {
    moveItemInArray(this.joueurJeux, event.previousIndex, event.currentIndex);
    //let clone =Object.assign({},this.joueurJeux);
    console.log(this.joueurJeux);
    let clone= this.joueurJeux;

    /*for(var i=0;i<clone.length;i++){
      console.log('joueurJeux/'+this.idAventure+'/'+clone[i].key+'/rang');
    }*/
  }
  enregistrerModification(){
    console.log(this.joueurJeux);
    let clone:any={};
    for(var i=0;i<this.joueurJeux.length;i++){
      this.joueurJeux[i].rang = i+1;
      clone[this.joueurJeux[i].key] = this.joueurJeux[i];
    }
    //console.log('clone',clone);
    this.tri(this.joueurJeux,'rang');
    this.joueurJeux.slice().reverse();
    this.db.database.ref('joueurJeux/'+this.idAventure+'/').set(clone).then(()=>{
      this.loadJoueurJeux('rang');
    });
  }

  
    open(content, joueur) {
          console.log(this.photos[joueur.key] );
          this.photo =  this.photos[joueur.key].toString();
          this.modalService.open(content).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          });
      }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}

