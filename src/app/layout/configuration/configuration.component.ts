import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AngularFireDatabase } from "angularfire2/database";
import { Jeux } from './../../shared/models/jeux.model';
import { Aventure } from './../../shared/models/aventure.model';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

	idAventure:String;
	loading:boolean=false;// afficher loader
	aventure:Aventure = new Aventure();
  imageSrc:string | ArrayBuffer;
  image:Blob;

  error:Boolean=false;
  message:string="";

  aventure_error:any[];
  	constructor(
  		public route: ActivatedRoute,
  		public router: Router,
  		public db:AngularFireDatabase
  		) {
  		this.idAventure = this.route.snapshot.paramMap.get('idAventures');  
  	}

  	ngOnInit() {
      this.message = ""
  		let self = this;
      this.loading = true;
	     this.db.object("aventures/"+this.idAventure).snapshotChanges().subscribe((data) =>{
	       console.log("AVENTURE %o", data.payload.val());
	       self.aventure = new Aventure().deserialize({ key: data.key, ...data.payload.val() });
	       this.loading = false;
	     })
  	}

  	contenu(){
	    this.router.navigate(['creerAventures/'+this.idAventure]);
	}

	leaderboard(){
	    this.router.navigate(['leaderboardAventure/'+this.idAventure]);
	}

  paiement(){
      this.router.navigate(['paiement/'+this.idAventure]);
  }

  prepareImageUpload(event){
      let self = this;
      if (event.target.files && event.target.files[0]) {
        this.image = event.target.files[0];
        const reader = new FileReader();
        reader.onload = e => self.imageSrc = reader.result;

        reader.readAsDataURL(this.image);
      }
  }

  updateAventure(){
    let self = this;
    
    this.aventure_error = [];
    this.error = false;
    let lang = false;
        for(let i in this.aventure.language){
          if(this.aventure.language[i]){
            lang = true;
            console.log("aventure %o", this.aventure);
            if(!this.aventure.titre[i]){
              this.aventure_error.push('Le titre '+i+' est obligatoire');
              
            }

            if(!this.aventure.description[i]){
              this.aventure_error.push('La description '+i+' est obligatoire');
              
            }
          }
        }
        if(!lang){
          this.aventure_error.push('La langue est obligatoire');
          
        }
    if(!this.aventure.titre){
      this.aventure_error.push('Le titre est obligatoire');
      this.error = true;
    }
    if(!this.aventure.description){
      this.aventure_error.push('La description est obligatoire');
      this.error = true;
    }
    if(!this.aventure.lieu){
      this.aventure_error.push('Le lieu est obligatoire');
      this.error = true;
    }
    if(!this.aventure.temps_min){
      this.aventure_error.push('Le temps minimum est obligatoire');
      this.error = true;
    }
    if(!this.aventure.temps_max){
      this.aventure_error.push('Le temps maximum est obligatoire');
      this.error = true;
    }
    if(!this.aventure.distance){
      this.aventure_error.push('La distance est obligatoire');
      this.error = true;
    }
    if(!this.aventure.niveau){
      this.aventure_error.push('Le niveau est obligatoire');
      this.error = true;
    }
    let nb_transport = 0;
    for(let t in this.aventure.transports){
      nb_transport +=1;
    }
    if(nb_transport == 0){
      this.aventure_error.push('Le transport est obligatoire');
      this.error = true;
    }

    if(!this.aventure.prix){
      this.aventure_error.push('Le prix est obligatoire');
      this.error = true;
    }

    
    console.log(this.error);
    if(!this.error){
      this.loading = true;
      this.aventure.status = 'valide';
      this.db.object("aventures/"+this.idAventure).set(this.aventure).then(function(){
        self.loading = false;
        window.scroll(0,0);
        self.message = "L'aventure a été validée."
      });
    }
      
    console.log(this.aventure);
  }

  disableAventure(){
      let self = this;
      this.loading = true;
      this.aventure.status = 'brouillon';
      this.db.object("aventures/"+this.idAventure).set(this.aventure).then(function(){
        self.loading = false;
        window.scroll(0,0);
        self.message = "L'aventure a été mise en brouillon."
      });

  }

}
