import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreeAventureComponent } from './cree-aventure.component';

describe('CreeAventureComponent', () => {
  let component: CreeAventureComponent;
  let fixture: ComponentFixture<CreeAventureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreeAventureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreeAventureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
