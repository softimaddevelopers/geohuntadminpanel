import { Component, OnInit,HostBinding } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { Router } from '@angular/router';
import { Jeux, Indices } from './../../shared/models/jeux.model';
import { Aventure } from './../../shared/models/aventure.model';
import { JeuxService } from './../../shared/services/jeux.service';
import * as firebase from 'firebase';
import { trigger,state,style,animate,transition,query, stagger, keyframes } from '@angular/animations';
import { AventureService } from './../../shared/services/aventure.service';
import { ImageCropperComponent,ImageCroppedEvent } from 'ngx-image-cropper';
import * as L from "leaflet";

@Component({
  selector: 'app-cree-aventure',
  templateUrl: './cree-aventure.component.html',
  styleUrls: ['./cree-aventure.component.scss'],
  animations: [
    // animation triggers go here
   trigger('openClose', [
      // ...
      state('open', style({
        opacity:0,
        display:'none'
      })),
      state('closed', style({
      	display:'block',
        height: '200px',
        opacity: 1,
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
   /*trigger('ShowHiddeAlert',[
   	state('show',style({
   		
   		display:'block'
   	})),
   	state('hidde',style({
   		left:'-3000px',
   		display:'none',
   	})),
   	transition('* => *',[animate('2s')])
   ])*/
  ]
})
export class CreeAventureComponent implements OnInit {
  closeResult: string;
  isOpen = true;
  penalisation_passer:number = null;
  requis:boolean = false;
  description:string;
  titre:string;
  file:Blob;
  GetNameFile:string;
  filename:string;

  idAventure:string;
  urlImagePuzzle:string;
  nomImage:string;
  idJeux:string;
  aventure:Aventure = new Aventure();
  imageSrc:string | ArrayBuffer;
  image:Blob;
  imageChangedEvent: any = '';
  croppedImage: any = '';

  loading:boolean=false;// afficher loader

  showAlert:boolean= false;

  quizz:Jeux;
  geo:Jeux;
  puzzle:Jeux;
  qrcode:Jeux;
  photo:Jeux;

  jeux:Jeux[];

  sequence:Number;
  error:any={};

  options:any;

  lat:number = 12.457633;
  lng:number = 104.923981;
  layers:any;



  constructor( 
  	private modalService: NgbModal,
  	public service:AventureService,
  	public route: ActivatedRoute,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    public router: Router,
    public jeuxService: JeuxService
  	) { }
  ngOnInit() {
    this.loading=true;
    this.idAventure = this.route.snapshot.paramMap.get('idAventures'); 

    let self = this;
     this.db.object("aventures/"+this.idAventure).snapshotChanges().subscribe((data) =>{
       console.log("AVENTURE %o", data.payload.val());
       self.aventure = new Aventure().deserialize({ key: data.key, ...data.payload.val() });

     })

     this.jeuxService.getJeux(this.idAventure).subscribe(items =>{
        let jeux = items.map(a => ({ key: a.key, ...a.payload.val() }));
        console.log("JEUX : %o", items);
        self.jeux = jeux.map( (j:Jeux) => { return new Jeux(self.aventure).deserialize(j) });
        self.jeux.sort(function compare(a,b) {
          if (a.sequence < b.sequence)
            return -1;
          if (a.sequence > b.sequence)
            return 1;
          return 0;
        })
        this.loading =false;
        console.log("JEUX : %o", self.jeux);
     })


     this.options = { layers: [
          L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
        ],
        zoom: 5
       
      }
      
     this.layers = [
        L.marker([ this.lat, this.lng], {
        icon: L.icon({
          iconSize: [ 25, 41 ],
          iconAnchor: [ 13, 41 ],
          iconUrl: '../../../assets/images/marker-icon.png',
          shadowUrl: 'leaflet/marker-shadow.png'
        })
      })
    ];

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          console.log("current position %o", position);
          self.lat = position.coords.latitude;
          self.lng = position.coords.longitude;

        });

      }else{
        alert("Geolocation is not supported by this browser.");
      }

   
    
  }
  
  centerMap(){
 

    if(isNaN(Number(this.geo.latitude)) && isNaN(Number(this.geo.longitude)) ){
      console.log("nan use %o %o",this.lat, this.lng);

      return L.latLng(Number(this.lat), Number(this.lng));
    }else{
      return L.latLng(Number(this.geo.latitude), Number(this.geo.longitude));
    }

    
  }
  creerPuzzle(){
  	this.puzzle.actif= true;
    if(this.croppedImage){
      this.puzzle.image_url = this.croppedImage;
    }
  	if(
      !this.puzzle.checkPuzzle()
      // !this.puzzle.titre || !this.puzzle.description || (!this.puzzle.requis && !this.puzzle.penalisation_passer) || (!this.croppedImage)){
  		){
      console.log('les informations sont incompletes');
  	}
  	else{
      let self = this;
      this.puzzle.actif = false;
      console.log(this.puzzle)
      if(this.puzzle.key){
          let puzzle = this.db.database.ref("jeux/"+this.puzzle.key).set(this.puzzle);
          if(self.file){
              self.uploadPuzzleImage(self.file, this.puzzle.key);
          }
      }else{
          this.puzzle.aventures_id = this.idAventure;
          this.puzzle.type = "puzzle";
          this.manageSequence();
          this.puzzle.sequence =  Number(this.sequence) +1;
          let puzzle = this.db.database.ref("jeux").push(this.puzzle)
          console.log("puzzle %o", puzzle);
          if(self.file){
              self.uploadPuzzleImage(self.file, puzzle.key);
          }
      }
      

      this.modalService.dismissAll();
	  	// 	let that =this;
	  	// 	this.puzzle.actif= false;
		  // 	if(this.imageChangedEvent!=null){
		  // 		that.loading = true;
		  // 	}
		  // 	this.modalService.dismissAll();
		  // 	console.log("puzzle : %o",this.puzzle);
		  //   if(this.puzzle.key){
		  //     this.idJeux = this.puzzle.key;
		  //   }else{
		  //     this.idJeux = this.service.create('/jeux', this.puzzle).key;
		  //   }

		  //   console.log("id puzzle : %o", this.idJeux);
		  	
		  // 	let task = this.service.pushFile('jeux/'+this.idJeux,this.file);
		  // 	 task.on(firebase.storage.TaskEvent.STATE_CHANGED,
		  // 		(snap) =>{
		  // 			console.log(snap);
		  // 		},
		  // 		(err) =>{
		  // 			console.log(err);
		  // 			that.loading = false;
		  // 		},
		  // 		() =>{
		  // 			task.snapshot.ref.getDownloadURL().then(
		  // 				(url) => {
		  // 					that.urlImagePuzzle= url;
		  // 					this.manageSequence();
    //             // this.puzzle.penalisation_passer = (this.puzzle.penalisation_passer) ? (this.puzzle.penalisation_passer != undefined) : this.penalisation_passer;
		  // 					this.puzzle.sequence = (this.puzzle.sequence) ? (this.puzzle.sequence) : Number(this.sequence)+1
    //      //        let data ={
				// 		  	// 	penalisation_passer:(this.puzzle.penalisation_passer)?(this.puzzle.penalisation_passer != undefined):this.penalisation_passer,
				// 		  	// 	requis:this.puzzle.requis,
				// 		  	// 	description:this.puzzle.description,
				// 		  	// 	titre:this.puzzle.titre,
				// 		  	// 	image_url:this.urlImagePuzzle,
				// 		  	// 	aventures_id:this.idAventure,
				// 		  	// 	sequence:(this.puzzle.sequence)?(this.puzzle.sequence):Number(this.sequence)+1,
				// 		  	// 	type:"puzzle"
				// 		  	// }
		  //           		// console.log("creating puzzle : %o", data);
    //             this.jeuxService.set('/jeux/'+this.idJeux+'/image_url',that.urlImagePuzzle);
				// 		  	this.jeuxService.set('/jeux/'+this.idJeux+'/sequence',this.puzzle.sequence);
				// 		  	that.loading = false;
				// 		  	console.log('vous creer puzzle');
		  // 				}
		  // 			);
		  // 		}
		  // 	);

		  //   this.modalService.dismissAll();
  		// //fin else
  	}

 
  }

  open(content) {
      this.quizz = new Jeux(this.aventure);
      this.quizz.type = "quizz";
      this.puzzle=new Jeux(this.aventure);
      this.imageChangedEvent=null;

      this.geo = new Jeux(this.aventure);
      this.geo.type = "geolocalisation";
      this.qrcode = new Jeux(this.aventure);
      this.qrcode.type = "qrcode";
      this.photo = new Jeux(this.aventure);
      this.photo.type = "photo";

      this.image = null;
      this.croppedImage = '';
        this.modalService.open(content, {size: 'lg'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    edit(content, jeux) {

      switch (jeux.type) {
        case "quizz":
          this.quizz = jeux
          break;
        case "geolocalisation":

          this.geo = jeux
          this.layers = [
            L.marker([ Number(this.geo.latitude), Number(this.geo.longitude)], {
                  icon: L.icon({
                    iconSize: [ 25, 41 ],
                    iconAnchor: [ 13, 41 ],
                    iconUrl: '../../../assets/images/marker-icon.png',
                    shadowUrl: 'leaflet/marker-shadow.png'
                  })
                })
            ];
          break;
        case "puzzle":
          this.puzzle = jeux;
          this.imageChangedEvent=null;
          this.croppedImage = jeux.image_url;
          console.log(this.puzzle);
          break;

        case "qrcode":
          this.qrcode = jeux
          break;

        case "photo":
          this.photo = jeux
          break;
        default:
          // code...
          break;
      }
        this.modalService.open(content, {size: 'lg'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
  

  addQuizz(){
  	this.quizz.actif = true;
    // if ( 
    // 	 !this.quizz.question_fr ||
	   //   !this.quizz.question_en ||
	   //   !this.quizz.reponse_fr || 
	   //   !this.quizz.reponse_en || 
	   //   (!this.quizz.requis && !this.quizz.penalisation_passer)|| 
	   //   !this.quizz.testIndicesParIndice() ||
	   //   !this.quizz.testIndicesParPenalisation()
	   //  ){
    // 	console.log('information non conformes');
    // }
    this.quizz.errors = [];
    if(!this.quizz.checkQuizz()){

    }
    else
    {
    	let self = this;
    	this.quizz.actif = false;
	    console.log(this.quizz)
	    if(this.quizz.key){
	        let quizz = this.db.database.ref("jeux/"+this.quizz.key).set(this.quizz);
	        if(self.image){
	            self.uploadImage(self.image, this.quizz.key);
	        }
	    }else{
	        this.quizz.aventures_id = this.idAventure;
	        this.quizz.type = "quizz";
	        this.manageSequence();
	        this.quizz.sequence =  Number(this.sequence) +1;
	        let quizz = this.db.database.ref("jeux").push(this.quizz)
	        console.log("QUIZZ %o", quizz);
	        if(self.image){
	            self.uploadImage(self.image, quizz.key);
	        }
	    }
	    

	    this.modalService.dismissAll();
    }

  }

  addGeo(){
    console.log(this.geo);
    this.geo.actif=true;
    if(
      !this.geo.checkGeoloc()
    	// !this.geo.latitude || 
    	// !this.geo.longitude || 
    	// (!this.geo.requis && !this.geo.penalisation_passer) ||
    	// !this.geo.testIndicesParIndice() ||
    	// !this.geo.testIndicesParPenalisation()
      ){
    	console.log('information non conformes');
    }
    else{
    	console.log("edit geo");
    	this.geo.actif=false;
	    let self = this;
	    if(this.geo.key){
	        let geo = this.db.database.ref("jeux/"+this.geo.key).set(this.geo);
	        if(self.image){
	            self.uploadImage(self.image, this.geo.key);
	        }
	    }else{
	        this.geo.aventures_id = this.idAventure;
	        this.geo.type = "geolocalisation";
	        this.manageSequence();
	        console.log("GEO %o", this.geo);
	        this.geo.sequence =  Number(this.sequence) +1;
	        let geo = this.db.database.ref("jeux").push(this.geo)
	        
	        if(self.image){
	            self.uploadImage(self.image, geo.key);
	        }
	    }
	      

	    this.modalService.dismissAll();
    }

  }

  addQrcode(){
    if(
      !this.qrcode.checkQrcode()
    	// !this.qrcode.texte_qrcode || 
    	// (!this.qrcode.requis && !this.qrcode.penalisation_passer)||
    	// !this.qrcode.testIndicesParIndice() ||
    	// !this.qrcode.testIndicesParPenalisation()
    ){
    	this.qrcode.actif=true;
    	console.log('information non conformes');
    }
    else{
    	console.log(this.qrcode);
	    console.log("edit qrcode %o", this.qrcode);
	    this.qrcode.actif =false;
	    let self = this;
	    if(this.qrcode.key){
	        let qrcode = this.db.database.ref("jeux/"+this.qrcode.key).set(this.qrcode).then((data)=>{
            console.log("success %o", data);
          });
	        if(self.image){
	            self.uploadImage(self.image, this.qrcode.key);
	        }
	    }else{
	        this.qrcode.aventures_id = this.idAventure;
	        this.qrcode.type = "qrcode";
	        this.manageSequence();
	        this.qrcode.sequence =  Number(this.sequence) +1;
	        let qrcode = this.db.database.ref("jeux").push(this.qrcode)
	        console.log("QRCODE %o", qrcode);
	        if(self.image){
	            self.uploadImage(self.image, qrcode.key);
	        }
	    }
	      

	    this.modalService.dismissAll();
    }
  }

  addPhoto(){
    if(
      !this.photo.checkPhoto()
    	// !this.photo.titre || 
    	// !this.photo.description || 
    	// (!this.photo.requis && !this.photo.penalisation_passer)||
    	// !this.geo.testIndicesParIndice() ||
    	// !this.geo.testIndicesParPenalisation()
    ){
    	this.photo.actif =true;
    	console.log('information non conformes');
    }
    else
    {
    	console.log(this.photo);
	    console.log("edit photo");
	    this.photo.actif= false;
	    let self = this;
	    if(this.photo.key){
	        let photo = this.db.database.ref("jeux/"+this.photo.key).set(this.photo);
	        if(self.image){
	            self.uploadImage(self.image, this.photo.key);
	        }
	    }else{
	        this.photo.aventures_id = this.idAventure;
	        this.photo.type = "photo";
	        this.manageSequence();
	        this.photo.sequence =  Number(this.sequence) +1;
	        let photo = this.db.database.ref("jeux").push(this.photo)
	        console.log("QRCODE %o", photo);
	        if(self.image){
	            self.uploadImage(self.image, photo.key);
	        }
	    }
	      

	    this.modalService.dismissAll();
    }
  }

  updateAventure(){
    this.db.object("aventures/"+this.idAventure).set(this.aventure);
    console.log(this.aventure);
    this.modalService.dismissAll();
  }

  prepareImageUpload(event){
      let self = this;
      if (event.target.files && event.target.files[0]) {

        this.image = event.target.files[0];
        this.filename = event.target.files[0].name;
        const reader = new FileReader();
        reader.onload = e => self.imageSrc = reader.result;

        reader.readAsDataURL(this.image);
      }
  }

  uploadImage(file:Blob, jeux_id){
     let self = this;
     this.loading= true;
     // this.blobToDataURL(file).then((value)=>{
     //      self.db.database.ref('jeux/'+jeux_id+"/image_url/").set(value).then(function(){
     //        self.loading = false;
     //      });
     // });


    let uploadTask = this.storage.ref('jeux/'+jeux_id+'/'+this.filename).put(file).then(function(snapshot){
        console.log('setting url');
        snapshot.ref.getDownloadURL().then(value =>{
            console.log(value);
            self.db.database.ref('jeux/'+jeux_id+"/image_url/").set(value).then(function(){
            	self.loading = false;
            })
       
        });
    });
  }

  uploadPuzzleImage(file:Blob, jeux_id){
     let self = this;
     this.loading= true;
     // this.blobToDataURL(file).then((value)=>{
     //      self.db.database.ref('jeux/'+jeux_id+"/image_url/").set(value).then(function(){
     //        self.loading = false;
     //      });
     // })
     
    let uploadTask = this.storage.ref('jeux/'+jeux_id+'/'+this.filename).put(file).then(function(snapshot){
        console.log('setting url');
        snapshot.ref.getDownloadURL().then(value =>{
            console.log(value);
            self.db.database.ref('jeux/'+jeux_id+"/image_url/").set(value).then(function(){
              self.loading = false;
            })
       
        });
    });
  }

  blobToDataURL(blob) {
    return new Promise((fulfill, reject) => {
        let reader = new FileReader();
        reader.onerror = reject;
        reader.onload = (e) => fulfill(reader.result);
        reader.readAsDataURL(blob);
    })
  }

  addIndice(jeux){
    console.log(jeux.type);
    switch (jeux.type) {
      case "quizz":
        this.quizz.indices.push(new Indices());
        break;
      case "geolocalisation":
        this.geo.indices.push(new Indices());
      case "qrcode":
        this.qrcode.indices.push(new Indices());
      case "photo":
        this.photo.indices.push(new Indices());
      default:
        // code...
        break;
    }
      }

  removeIndice(jeux,i){
    switch (jeux.type) {
      case "quizz":
        this.quizz.indices.splice(i,1);
        break;
      case "geolocalisation":
        this.geo.indices.splice(i,1);
        break;

      case "qrcode":
        this.qrcode.indices.splice(i,1);
        break;
      case "photo":
        this.photo.indices.splice(i,1);
        break;
      
      default:
        // code...
        break;
    }
    
  }

  configuration(){
    console.log("here");
    this.router.navigate(['configurationAventure/'+this.idAventure]);
  }

  leaderboard(){
      this.router.navigate(['leaderboardAventure/'+this.idAventure]);
  }

  paiement(){
      this.router.navigate(['paiement/'+this.idAventure]);
  }

  clickMap(event){
    console.log("CLICK MAP %o", event);
    this.lat = event.latlng.lat;
    this.lng = event.latlng.lng;
    this.geo.latitude = this.lat;
    this.geo.longitude = this.lng;

    this.options = { layers: [
          L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
        ],
        zoom: 5,
        center: L.latLng(this.lat, this.lng)
      }
    this.layers = [
        L.marker([ this.lat, this.lng], {
        icon: L.icon({
          iconSize: [ 25, 41 ],
          iconAnchor: [ 13, 41 ],
          iconUrl: '../../../assets/images/marker-icon.png',
          shadowUrl: 'leaflet/marker-shadow.png'
        })
      })
    ];

    console.log(this.lat);
    console.log(this.lng);
  }

  manageSequence(){
    // check after
    let self = this;
    let jeux_after_current = this.jeux.filter( item =>  item.sequence >  self.sequence);
    console.log(jeux_after_current);
    jeux_after_current.forEach((child) => {
      console.log("setting : %o", child);
      console.log("setting child to  : %o", Number(child.sequence) + 1);
      self.db.object("jeux/"+child.key+"/sequence").set(Number(child.sequence) + 1);
    }) 
  }

  changeIsOpenToTrue(){
    this.isOpen=true;
  }
  toggle(sequence=0) {
    this.isOpen = !this.isOpen;
    this.sequence = sequence;
  }
  detectFile(ev){
    this.GetNameFile = ev.target.files[0];
    this.filename = ev.target.files[0].name;
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    this.file = event.file;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  cancel(){
    this.isOpen = false;
  }


}
