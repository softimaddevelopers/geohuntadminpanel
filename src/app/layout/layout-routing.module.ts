import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AventuresComponent } from './aventures/aventures.component';
import { CreeAventureComponent } from './cree-aventure/cree-aventure.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PagesComponent } from './pages/pages.component';
import { PaiementComponent } from './paiement/paiement.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'aventures', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'aventures', component: AventuresComponent },
            { path: 'creerAventures/:idAventures', component: CreeAventureComponent },
            { path: 'configurationAventure/:idAventures', component: ConfigurationComponent },
            { path: 'leaderboardAventure/:idAventures', component: LeaderboardComponent },
            { path: 'paiement/:idAventures', component: PaiementComponent },
            { path: 'pages', component: PagesComponent },
     
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
