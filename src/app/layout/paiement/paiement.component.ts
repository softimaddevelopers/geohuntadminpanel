import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AngularFireDatabase } from "angularfire2/database";
import { Jeux } from './../../shared/models/jeux.model';
import { Aventure } from './../../shared/models/aventure.model';
import { JoueurService } from './../../shared/services/joueur.service';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {
	idAventure:String;
	loading:boolean=false;// afficher loader
	aventure:Aventure = new Aventure();
  paiements:any[];

  constructor(
  	public route: ActivatedRoute,
  		public router: Router,
  		public db:AngularFireDatabase,
      public joueurService: JoueurService
  	) {

  		this.idAventure = this.route.snapshot.paramMap.get('idAventures'); 
   }

  ngOnInit() {
      this.loading = true;
      let self = this;
       this.db.object("aventures/"+this.idAventure).snapshotChanges().subscribe((data) =>{
         console.log("AVENTURE %o", data.payload.val());
         self.aventure = new Aventure().deserialize({ key: data.key, ...data.payload.val() });
         

       });
       self.joueurService.getPaiements(self.idAventure).subscribe((paiements)=>{
         self.loading = false;
             self.paiements = paiements.map((a:any) => ({ key: a.key, date:a.payload.val().method == "stripe" ? new Date(a.payload.val().stripe_info.created):new Date(a.payload.val().paypal_info.response.create_time),   ...a.payload.val() }));
      
             console.log("PAIEMENTS %o", self.paiements);
             console.log(this.loading);
             

       })
  }

  contenu(){
      this.router.navigate(['creerAventures/'+this.idAventure]);
  }
  configuration(){
      this.router.navigate(['configurationAventure/'+this.idAventure]);
  }
  leaderboard(){
      this.router.navigate(['leaderboardAventure/'+this.idAventure]);
  }

}
