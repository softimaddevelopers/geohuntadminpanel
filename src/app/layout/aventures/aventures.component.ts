import { Component, OnInit } from '@angular/core';
import { PageHeaderModule } from './../../shared';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';


import { AngularFireStorage } from "angularfire2/storage";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";


import { Aventure, AventureError } from '../../shared/models/aventure.model';
import { AventureService } from '../../shared/services/aventure.service';






import {NgForm} from '@angular/forms';
 @Component({
  selector: 'app-aventures',
  templateUrl: './aventures.component.html',
  styleUrls: ['./aventures.component.scss']
})
export class AventuresComponent implements OnInit {
  pathData:string='/aventures';
  titre:string;
  description:string;
  temps_min:number;
  temps_max:number;
  niveau:string;
  idAventures:string;
  test:string = "balbala";
  avatar:Blob;
  imageSrc:any;
  aventure: Aventure = new Aventure();
  aventures:Aventure[];
  plus:boolean =false;
  loading:boolean=false;// afficher loader

  aventureForm: FormGroup;

  error:AventureError = new AventureError();
  constructor(
    private modalService: NgbModal,
    public router:Router,
    public db: AngularFireDatabase,
    public storage: AngularFireStorage,
    public service: AventureService,

    ) { 
    

  }

  closeResult: string; 
  ngOnInit() {
      let that = this;

      // this.aventureForm = this.formBuilder.group({
      //     titre: ['', Validators.required],
      //     description: ['', Validators.required],
      //     temps_max: ['', Validators.required],
      //     temps_min: ['', Validators.required],
      //     distance: ['', Validators.required],
      //     niveau: ['', Validators.required],
      // })

      // this.db.database.ref("aventures").on("value", function(snapshot){
            
      //       snapshot.forEach( function(child){
      //         that.aventures.push(child.val());
      //       })
            
           
      //       that.test = "sfbdskf";
      //       console.log(that.aventures);
            
      //  })
      this.loading = true;
      this.service.getAventures().subscribe((data) =>{
          let aventures = data.map(a => ({ key: a.key, ...a.payload.val() }));
          
          that.aventures = aventures.map((a:Aventure) => new Aventure().deserialize(a));
          this.loading = false;
          console.log(that.aventures);
      })
   
  }

  // convenience getter for easy access to form fields
  get f() { return this.aventureForm.controls; }

  open(content) {
        this.error= new AventureError();
        this.modalService.open(content, {size: 'lg'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });

  }
  /*setToBoolean(error:any){
      error = true;
      console.log(error);
  }*/

  creer(){
    this.error = new AventureError();
    let error = false;

       
        let lang = false;
        for(let i in this.aventure.language){
          if(this.aventure.language[i]){
            lang = true;

            if(!this.aventure.titre[i]){
              this.error.titre[i] = true;
              error = true;
            }

            if(!this.aventure.description[i]){
              this.error.description[i] = true;
              error = true;
            }
          }
        }
        if(!lang){
          this.error.language= true;
          error = true;
        }

    console.log(error);
    if(!error){


      console.log(this.avatar);
      let self = this;
      var key = this.service.create(this.pathData,this.aventure).on('value',snapshot=>{
         self.idAventures = snapshot.key;
         console.log(snapshot.key);
         if(self.avatar){
           self.upload(self.avatar, snapshot.key);
         }
   
        this.router.navigate(['creerAventures/'+this.idAventures]);
        this.modalService.dismissAll();
      })
    }  
  }

  delete(aventure:Aventure){
    if(aventure.status == 'valide'){
      alert('Vous ne pouvez supprimer une aventure valide')
    }else{
      let r = confirm("Voulez vous supprimer l'aventure "+aventure.titre.fr +"?");
      if(r){
        this.loading = true;
        let self = this;
        this.service.delete(aventure.key).then(function(){
          self.loading = false;
        });  
      }
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
    } else {
            return  `with: ${reason}`;
    }

        
  }

  edit(id){
    this.router.navigate(['creerAventures/'+id]);
  }

  prepareUpload(event){
      let self = this;
      if (event.target.files && event.target.files[0]) {
        this.avatar = event.target.files[0];
        const reader = new FileReader();
        reader.onload = e => self.imageSrc = reader.result;

        reader.readAsDataURL(this.avatar);
      }
  }

  upload(file, aventure_id){
    let self = this;
    let uploadTask = this.storage.ref('aventures/'+aventure_id+'/avatar/').put(file).then(function(snapshot){
        console.log('setting url');
        snapshot.ref.getDownloadURL().then(value =>{
            console.log(value);
            self.db.database.ref('aventures/'+aventure_id+"/avatar_url/").set(value).then(function(){

            })
       
        });
    });


  }

}
