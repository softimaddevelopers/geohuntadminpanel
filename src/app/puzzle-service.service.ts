import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PuzzleServiceService {
  
  task: firebase.storage.UploadTask;
  constructor(
    public db :AngularFireDatabase
    ) {}
  getData(path:string){
  	return this.db.database.ref(path)
  }
  create(path:string,data:Object=null){
  	 return this.db.list(path).push(data)
  }
  update(path:string,id:string,data:Object){
  	return this.db.list(path).update(id, data);
  }
  set(path:string,id:string,data:Object){
    return this.db.list(path).set(id, data);
  }
  delete(path:string,id:string){
  	return this.db.list(path+'/'+id).remove();
  }
  pushFile(path :string ='/jeux',file:File){
    let sotrageRef = firebase.storage().ref();
    this.task = sotrageRef.child(path+'/'+file.name).put(file);
    return this.task;
  }
}
